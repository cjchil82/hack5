class CreateDonors < ActiveRecord::Migration
  def change
    create_table :donors do |t|
      t.string :First_Name
      t.string :Last_Name
      t.string :Email

      t.timestamps
    end
  end
end
