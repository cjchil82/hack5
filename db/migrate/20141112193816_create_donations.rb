class CreateDonations < ActiveRecord::Migration
  def change
    create_table :donations do |t|
      t.date :Date
      t.decimal :Amount
      t.integer :Donor_id

      t.timestamps
    end
  end
end
